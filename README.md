### Sample Work

1. What is Sample Work?
2. Requirements
3. Installation and creating jar
4. Running the application
5. Notes and observations

\-------------------------------------

##### 1. What is Sample Work?

This is a sample work which is developed according to Helmes sample work requirements.
This java project is based on basic setup from https://github.com/spring-guides/tut-react-and-spring-data-rest/tree/master 

It's using Spring-Boot as a backend and React on a frontend side. 
Session holding is done by Spring-Security. 
Sample Work is using H2 database.

\-------------------------------------

##### 2. Requirements

* JDK 8
* Maven >= 3.0.4
* Node.js >= 6.10.2

\-------------------------------------

##### 3. Installation and creating jar

1. Open project in IntelliJ..
2. Set Project SDK to JDK 8.
3. (IntelliJ Idea) Enable annotation processing
    * Settings -> Annotation Processors
    * Check Enable annotation processing
4. Start install command under *Sample Work* module. It will take time because of downloading node_modules. With command: `npm install`
5. (Optional) When react components are updated run: `npm run watch`

\-------------------------------------

##### 4. Running the application (after install)

1. Run main from SampleWorkApplication.java
2. Application should be running at localhost:8080

\-------------------------------------

##### 5. Notes and observations

* Lombok is used on entities to generate getters-setters.
* Hibernate generates columns in alphabetical order.

Another possible solutions:

1. Session holding in react (updating object using user id) and no sessions in backend

For the future:

* Unit tests for backend and frontend
* OOP(structure) on the frontend side
* Button for ending the session and session timeout
* Database generated with liquibase scripts

\-------------------------------------
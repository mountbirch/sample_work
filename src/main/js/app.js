'use strict';
import 'antd/dist/antd.css';
import {Form, Icon, Input, Checkbox, Button, TreeSelect, message} from 'antd';
import axios from 'axios';

// tag::vars[]
const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
const FormItem = Form.Item;
const TreeNode = TreeSelect.TreeNode;
// end::vars[]

// tag::app[]
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sectors: [],
            name: '',
            agreeToTerms: false,
            selected: undefined,
            nameInputStatus: '',
            errorMsg: ''
        };
    }

    componentDidMount() {
        client({method: 'GET', path: '/api/sectors/rootSectors'}).done(response => {
            this.setState({sectors: this.getSectorStructure(response.entity)});
        });

        client({method: 'GET', path: '/api/users'}).done(response => {
            if (typeof response.entity !== 'undefined') {
                this.setState({name: response.entity.name});
                if (typeof response.entity.name !== 'undefined') {
                    this.setState({agreeToTerms: true});
                }
                let selected = [];
                if (typeof response.entity.sectors !== 'undefined') {
                    response.entity.sectors.forEach((sector) => {
                        selected.push(sector.id.toString());
                    });
                }
                this.setState({selected: selected})
            }
        });
    }

    getSectorStructure = (children) => {
        let nodes = [];
        for (let i = 0; i < children.length; i++) {
            let node = {
                label: children[i].name,
                value: children[i].id.toString(),
                key: children[i].id
            };
            if (Array.isArray(children[i].subSectors)) {
                node['children'] = this.getSectorStructure(children[i].subSectors);
            }
            nodes.push(node);
        }
        return nodes;
    };

    handleNameChange = (e) => {
        let value = e.target.value;
        this.setState({name: value});
    };

    onTreeSelectChange = (value) => {
        this.setState({selected: value});
    };

    onChange = (e) => {
        let value = e.target.checked;
        this.setState({agreeToTerms: value});
    };

    handleSubmit = (e) => {
        e.preventDefault();

        if (this.validate()) {
            console.log(JSON.stringify(this.state));

            let data = new FormData();
            data.append('name', this.state.name);
            data.append('agreeToTerms', this.state.agreeToTerms);
            data.append('sectorIds', this.state.selected);

            axios.post('/api/users', data, {}).then(function (response) {
                console.log(response);
                message.success("User info saved");
            }).catch(function (error) {
                console.log(error);
                message.error(error.message);
            });
        }
    };

    validate = () => {
        if ((typeof this.state.name !== 'undefined') && (this.state.name.length > 2)) {
            this.setState({nameInputStatus: ''});
            this.setState({errorMsg: ''});
            return true;
        }
        this.setState({nameInputStatus: 'error'});
        this.setState({errorMsg: 'Name must be at least 3 characters'});
        return false;
    };

    render() {
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 5},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 12},
            },
        };

        return (
            <Form onSubmit={this.handleSubmit} style={{maxWidth: '600px'}}>
                <FormItem {...formItemLayout}
                          label="Name"
                          validateStatus={this.state.nameInputStatus}
                          help={this.state.errorMsg}>
                    <Input value={this.state.name} onChange={this.handleNameChange}/>
                </FormItem>

                <FormItem {...formItemLayout} label="Sectors">
                    <TreeSelect placeholder="Select Sectors"
                                allowClear
                                multiple
                                value={this.state.selected}
                                onChange={this.onTreeSelectChange}
                                treeData={this.state.sectors}
                                treeDefaultExpandAll>
                    </TreeSelect>
                </FormItem>

                <FormItem {...formItemLayout}>
                    <Checkbox checked={this.state.agreeToTerms} onChange={this.onChange}>Agree to terms</Checkbox>
                </FormItem>

                <FormItem>
                    <Button type="primary" htmlType="submit" className="login-form-button"
                            disabled={!this.state.agreeToTerms}>
                        Save
                    </Button>
                </FormItem>
            </Form>
        )
    }
}

// end::app[]

// tag::render[]
ReactDOM.render(
    <App/>,
    document.getElementById('react')
);
// end::render[]


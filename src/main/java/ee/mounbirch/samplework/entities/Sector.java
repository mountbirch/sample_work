package ee.mounbirch.samplework.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
public class Sector {

	@Id @GeneratedValue
	private Long id;
	private String name;
	@OneToMany
	private List<Sector> subSectors = new LinkedList<>();

	public Sector(String name) {
		this.name = name;
	}
}
package ee.mounbirch.samplework;

import ee.mounbirch.samplework.entities.Sector;
import ee.mounbirch.samplework.repositories.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

	private final SectorRepository sectorRepository;

	@Autowired
	public DatabaseLoader(SectorRepository sectorRepository) {
		this.sectorRepository = sectorRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		sectorRepository.save(new Sector("Service"));
		Sector manufacturing = new Sector("Manufacturing");
		manufacturing.getSubSectors().add(sectorRepository.save(new Sector("Construction materials")));
		manufacturing.getSubSectors().add(sectorRepository.save(new Sector("Electronics and Optics")));
		sectorRepository.save(manufacturing);
	}
}
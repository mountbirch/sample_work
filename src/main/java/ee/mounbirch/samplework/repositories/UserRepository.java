package ee.mounbirch.samplework.repositories;

import ee.mounbirch.samplework.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}

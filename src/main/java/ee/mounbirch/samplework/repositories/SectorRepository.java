package ee.mounbirch.samplework.repositories;

import ee.mounbirch.samplework.entities.Sector;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SectorRepository extends CrudRepository<Sector, Long> {

    @Query(value = "SELECT * FROM SECTOR " +
            "WHERE ID NOT IN (SELECT SUB_SECTORS_ID FROM SECTOR_SUB_SECTORS)", nativeQuery =  true)
    List<Sector> findRootSectors();

    List<Sector> findAllByIdIn(Long[] ids);

}

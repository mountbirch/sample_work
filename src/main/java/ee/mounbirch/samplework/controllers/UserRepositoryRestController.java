package ee.mounbirch.samplework.controllers;

import ee.mounbirch.samplework.entities.Sector;
import ee.mounbirch.samplework.entities.User;
import ee.mounbirch.samplework.repositories.SectorRepository;
import ee.mounbirch.samplework.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@RepositoryRestController
@RequestMapping("/users")
public class UserRepositoryRestController {

    private static final String USER_ID_SESSION_ATTRIBUTE = "userId";
    private final UserRepository userRepository;
    private final SectorRepository sectorRepository;

    @Autowired
    public UserRepositoryRestController(UserRepository userRepository, SectorRepository sectorRepository) {
        this.userRepository = userRepository;
        this.sectorRepository = sectorRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getUserFromSession(HttpSession session) {
        return ResponseEntity.status(HttpStatus.OK).body(getUserFromRepositoryBySessionAttribute(session));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addOrUpdateUser(
            @RequestParam("name") String name,
            @RequestParam("agreeToTerms") boolean agreeToTerms,
            @RequestParam("sectorIds") Long[] sectorIds,
            HttpSession session
    ) {
        String validationErrorMessage = validateRequestData(name, agreeToTerms);
        if (validationErrorMessage != null) {
            return ResponseEntity.badRequest().body(validationErrorMessage);
        }

        User userInSession = getUserFromRepositoryBySessionAttribute(session);
        List<Sector> sectors = sectorRepository.findAllByIdIn(sectorIds);

        if (userInSession == null) {
            User user = new User();
            user.setName(name);
            user.getSectors().addAll(sectors);
            Long userId = userRepository.save(user).getId();
            session.setAttribute(USER_ID_SESSION_ATTRIBUTE, userId);
        } else {
            userInSession.setName(name);
            userInSession.getSectors().clear();
            userInSession.getSectors().addAll(sectors);
            userRepository.save(userInSession);
        }

        return ResponseEntity.status(HttpStatus.OK).body(true);
    }

    private User getUserFromRepositoryBySessionAttribute(HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID_SESSION_ATTRIBUTE);
        if(userId==null)return null;
        return userRepository.findOne(userId);
    }

    private String validateRequestData(String name, boolean agreeToTerms) {
        if (!agreeToTerms) {
            return "User has not agreed to terms";
        } else if (name.length() < 3) {
            return "User name must be longer. Minimum length is 3";
        }
        return null;
    }

}

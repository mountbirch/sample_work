package ee.mounbirch.samplework.controllers;

import ee.mounbirch.samplework.entities.Sector;
import ee.mounbirch.samplework.repositories.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RepositoryRestController
@RequestMapping("/sectors")
public class SectorRepositoryRestController {

  private final SectorRepository sectorRepository;

  @Autowired
  public SectorRepositoryRestController(SectorRepository sectorRepository) {
    this.sectorRepository = sectorRepository;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/rootSectors")
  public ResponseEntity getRootSectors() {
    Iterable<Sector> sectors = sectorRepository.findRootSectors();
    return ResponseEntity.status(HttpStatus.OK).body(sectors);
  }

}
